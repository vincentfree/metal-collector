cd ..
set version=%1
docker tag vincentfree/publish:latest vincentfree/publish:%version%
docker tag vincentfree/retrieval:latest vincentfree/retrieval:%version%
docker push vincentfree/publish:%version% && docker push vincentfree/retrieval:%version%
cd scripts