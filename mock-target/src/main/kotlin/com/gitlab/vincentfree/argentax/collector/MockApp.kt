package com.gitlab.vincentfree.argentax.collector

import com.gitlab.vincentfree.argentax.collector.constants.Currency
import com.gitlab.vincentfree.argentax.collector.constants.MetalType
import com.gitlab.vincentfree.argentax.collector.metals.Metal
import io.vertx.core.AbstractVerticle
import io.vertx.core.Vertx
import io.vertx.core.json.JsonArray
import io.vertx.ext.web.Router
import org.slf4j.LoggerFactory
import kotlin.random.Random

fun main(args: Array<String>) {
    val vertx = Vertx.vertx()
    val verticle = MockApp()
    vertx.deployVerticle(verticle)
}

class MockApp : AbstractVerticle() {

    override fun start() {
        log.info("application started")
        val router = Router.router(vertx)
        router.get("/metal").handler { context ->
            context.response().end(randomMetalList())
        }
        vertx.createHttpServer().requestHandler(router).listen(8080)
    }

    fun randomMetalList(): String {
        var x: Int = 0
        val metalList = mutableListOf<Metal>()
        while (x < 20) {
            metalList.add(randomMetal())
            x++
        }
        return JsonArray(metalList).encode()
    }

    fun randomMetal(): Metal {
        val price: Double = "%.2f".format(Random.nextDouble(0.0, 50.0)).replace(",", ".").toDouble()
        val metalTypNumber = Random.nextInt(0, 4)
        val currencyType = Random.nextInt(0, 3)

        val metalType = when (metalTypNumber) {
            0 -> MetalType.Gold
            1 -> MetalType.Silver
            2 -> MetalType.Platinum
            else -> MetalType.Palladium
        }

        val currency = when (currencyType) {
            0 -> Currency.EUR
            1 -> Currency.GBP
            else -> Currency.USD
        }
        return Metal(metalType, price, currency, System.currentTimeMillis())
    }

    companion object {
        private val log = LoggerFactory.getLogger(MockApp::class.java)
    }
}