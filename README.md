# Reactive metal collector

This project serves two purposes, on the one hand I like to explore 
Vertx's possibilities and on the other I'm making a application to pass through information.

The project scrapes and endpoint serving JSON data about precious metals.
The data is send over a `eventbus` from the scraper to a http server who will serve the information and store it in memory and later in a DB. 

The application consists of three small applications and a clustered eventbus.
the eventbus uses **Hazelcast** as a backend. The eventbus also has a **distributed Map** and **Service discovery** functionality

The three applications are

| application name | description |
|-----|-----|
| mock-target | a very small mock server that serves random precious metal information in json |
| retrieval | the application that scrapes the mock server |
| publish | the application that serves the scraped data |