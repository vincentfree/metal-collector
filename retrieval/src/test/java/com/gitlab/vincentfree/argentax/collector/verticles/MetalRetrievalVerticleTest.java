package com.gitlab.vincentfree.argentax.collector.verticles;

import com.gitlab.vincentfree.argentax.collector.constants.Currency;
import com.gitlab.vincentfree.argentax.collector.constants.MetalType;
import com.gitlab.vincentfree.argentax.collector.metals.Metal;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import io.vertx.ext.unit.junit.RunTestOnContext;
import io.vertx.ext.unit.junit.VertxUnitRunner;
import io.vertx.reactivex.core.Vertx;
import io.vertx.reactivex.core.eventbus.MessageConsumer;
import io.vertx.reactivex.core.http.HttpServer;
import io.vertx.reactivex.core.http.HttpServerResponse;
import io.vertx.reactivex.ext.web.Router;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@RunWith(VertxUnitRunner.class)
public class MetalRetrievalVerticleTest {
    private static final Logger log = LoggerFactory.getLogger(MetalRetrievalVerticleTest.class);

    @Rule
    public RunTestOnContext rule = new RunTestOnContext();
    private MetalRetrievalVerticle retrievalVerticle;

    @Before
    public void setUp(TestContext context) {
        retrievalVerticle = new MetalRetrievalVerticle();
        rule.vertx().exceptionHandler(context.exceptionHandler());

        //DeploymentOptions options = new DeploymentOptions();
        //options.setConfig(new JsonObject().put("port", 8080).put("host", "localhost").put("url", "/test").put("delay", 15000L));
        //deployVerticle(retrievalVerticle);
    }

    @Test
    public void CheckRetrievalVerticle(TestContext context) throws Exception {
        Async setUpServerAsync = context.async();
        Vertx vertx = new Vertx(rule.vertx());
        System.setProperty("hazelcastConfig", "true");
        vertx.getOrCreateContext().config().put("port", 8080).put("host", "localhost").put("url", "/test").put("delay", 500L);
        HttpServer server = vertx.createHttpServer();
        retrievalVerticle.init(vertx.getDelegate(), vertx.getOrCreateContext().getDelegate());
        retrievalVerticle.initialize();
        JsonArray expected = new JsonArray();

        Router router = Router.router(vertx);

        router.route(HttpMethod.GET, "/test").produces("Application/json").handler(event -> {
            HttpServerResponse response = event.response();
            Metal gold = new Metal(MetalType.Gold, 25.0, Currency.EUR, System.currentTimeMillis());
            JsonArray result = new JsonArray();
            result.add(new JsonArray().add(JsonObject.mapFrom(gold)));
            result.add(new JsonArray().add(JsonObject.mapFrom(gold.copy(gold.getType(), 21.0D, gold.getCurrency(), gold.getLogTime()))));
            expected.add(result);
            response.end(new JsonArray().add(gold.toJson()).encode());
        });

        server.exceptionHandler(err -> log.error("Internal error setting up connection", err));
        server.requestHandler(router);
        server.exceptionHandler(err -> context.fail(err.getMessage()));
        server.rxListen(8080)
                .subscribe(httpServer -> log.info("subscribed to http server")
                        , err -> log.error("failed to process request", err));


        //retrievalVerticle.start(Future.future());


        setUpServerAsync.complete();
        Async retrieveAsync = context.async();
        MessageConsumer<JsonArray> messageConsumer = vertx.eventBus().consumer("metal-json");
        messageConsumer.exceptionHandler(err -> context.fail(err.getMessage()));
        messageConsumer.handler(metal -> {
            log.info("Body: {}", metal.body());
            boolean result = expected.stream()
                    .filter(JsonArray.class::isInstance)
                    .map(JsonArray.class::cast)
                    .allMatch(json -> json.stream()
                            .filter(JsonArray.class::isInstance)
                            .map(JsonArray.class::cast)
                            .anyMatch(o -> o.encode().equals(metal.body().encode()))
                    );
            context.assertTrue(result);
        });

        retrievalVerticle.subscribeToEndpoint();

        vertx.setTimer(2000, event -> {
            JsonArray expectedSilver = new JsonArray().add(JsonObject.mapFrom(new Metal(MetalType.Silver, 10.1, Currency.EUR, System.currentTimeMillis())));
            expected.add(expectedSilver);
            rule.vertx().eventBus().publish("metal-json", expectedSilver);

            retrieveAsync.complete();
            retrievalVerticle.subscribeToEndpoint();
        });

    }
}
