package com.gitlab.vincentfree.argentax.collector;

import com.gitlab.vincentfree.argentax.collector.verticles.MetalRetrievalVerticle;
import com.hazelcast.config.Config;
import io.reactivex.Single;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import io.vertx.core.DeploymentOptions;
import io.vertx.core.Verticle;
import io.vertx.core.VertxOptions;
import io.vertx.core.json.JsonObject;
import io.vertx.core.spi.cluster.ClusterManager;
import io.vertx.reactivex.core.RxHelper;
import io.vertx.reactivex.core.Vertx;
import io.vertx.spi.cluster.hazelcast.ConfigUtil;
import io.vertx.spi.cluster.hazelcast.HazelcastClusterManager;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

/**
 * VertxApplication
 */
public class VertxApplication {

    private static final Logger log = LoggerFactory.getLogger(VertxApplication.class);
    private static CompositeDisposable compositeDisposable = new CompositeDisposable();
    private static Map<String, String> shutdownIds = new HashMap<>();

    @SuppressWarnings("Duplicates")
    public static void main(String[] args) {
        log.info("Initiating...");
        System.setProperty("vertx.hazelcast.config", "./cluster.xml");
        Config hazelcastConfig = ConfigUtil.loadConfig();
        if (System.getenv().keySet().stream().anyMatch(s -> s.equalsIgnoreCase("hazelcastlocal"))) {
            // TODO non multicast manager
            hazelcastConfig = new Config().setProperty("hazelcast.logging.type", "slf4j");
        }
        ClusterManager mgr = new HazelcastClusterManager(hazelcastConfig);
        VertxOptions options = new VertxOptions().setClusterManager(mgr);
        Vertx vertx = Vertx.rxClusteredVertx(options).blockingGet();

        JsonObject retrievalOptions = new JsonObject();
        retrievalOptions.put("url", getEndpoint().orElse("/metal"))
        .put("host", getHost().orElse("localhost"))
        .put("port", getPort().orElse(8080))
        .put("delay",getDelay().orElse(15000L));

        log.debug(retrievalOptions.encodePrettily());

        DeploymentOptions metalRetrievalOptions = new DeploymentOptions().setConfig(retrievalOptions);
        Verticle metalRetrievalVerticle = new MetalRetrievalVerticle();
        Single<String> metalRetrievalDeployment = RxHelper.deployVerticle(vertx, metalRetrievalVerticle,
                metalRetrievalOptions);

        startVerticle("metalRetrieval", metalRetrievalDeployment);

        final Thread mainThread = Thread.currentThread();
        Runtime.getRuntime().addShutdownHook(new Thread() {
            public void run() {
                log.info("Shutting down...");
                vertx.undeploy(shutdownIds.get("metalRetrieval"));
                compositeDisposable.dispose();
                log.info("Finished shutting down...");
                try {
                    mainThread.join();
                } catch (InterruptedException e) {
                    mainThread.interrupt();
                }
            }
        });
    }

    private static void startVerticle(String name, Single<String> deployment) {
        compositeDisposable.add(deployment.doOnSuccess(log::debug).subscribe(onSuccess(name), onError(name)));
    }

    @NotNull
    private static Consumer<Throwable> onError(String name) {
        return err -> log.error(name + " verticle failed to deploy!", err);
    }

    @NotNull
    private static Consumer<String> onSuccess(String name) {
        return id -> {
            log.info("started verticle with id: {}", id);
            shutdownIds.put(name, id);
        };
    }

    private static Optional<String> getEnv(String env) {
        if (envAvailable(env)) {
            return Optional.of(System.getenv(env));
        } else {
            return Optional.empty();
        }
    }

    private static Optional<String> getHost() {
        return VertxApplication.getEnv("hostVar");
    }
    
    private static Optional<String> getEndpoint() {
        return VertxApplication.getEnv("endpointVar");
    }

    private static Optional<Integer> getPort() {
        return VertxApplication.getEnv("portVar").filter(VertxApplication::isInteger).map(Integer::parseInt);
    }

    private static Optional<Long> getDelay() {
        return VertxApplication.getEnv("delayVar").filter(VertxApplication::isLong).map(Long::parseLong);
    }

    private static boolean envAvailable(String env) {
        return System.getenv().keySet().contains(env);
    }

    private static boolean isInteger(String s) {
        try {
            Integer.parseInt(s);
        } catch (NumberFormatException | NullPointerException e) {
            return false;
        }
        // only got here if we didn't return false
        return true;
    }

    private static boolean isLong(String s) {
        try {
            Long.parseLong(s);
        } catch (NumberFormatException | NullPointerException e) {
            return false;
        }
        // only got here if we didn't return false
        return true;
    }
}