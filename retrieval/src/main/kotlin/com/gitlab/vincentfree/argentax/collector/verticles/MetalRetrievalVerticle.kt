package com.gitlab.vincentfree.argentax.collector.verticles

import io.reactivex.Single
import io.reactivex.disposables.CompositeDisposable
import io.vertx.core.Future
import io.vertx.core.json.JsonArray
import io.vertx.kotlin.core.json.json
import io.vertx.kotlin.core.json.obj
import io.vertx.kotlin.servicediscovery.ServiceDiscoveryOptions
import io.vertx.reactivex.core.AbstractVerticle
import io.vertx.reactivex.ext.web.client.HttpResponse
import io.vertx.reactivex.ext.web.client.WebClient
import io.vertx.reactivex.ext.web.codec.BodyCodec
import io.vertx.reactivex.servicediscovery.ServiceDiscovery
import io.vertx.reactivex.servicediscovery.types.MessageSource
import org.slf4j.LoggerFactory

class MetalRetrievalVerticle : AbstractVerticle() {
    private val compositeDisposable = CompositeDisposable()
    private val bus by lazy { vertx.eventBus() }
    private val webClient: WebClient by lazy { WebClient.create(vertx) }
    private val discovery by lazy {
        ServiceDiscovery.create(vertx, ServiceDiscoveryOptions()
                .setAnnounceAddress("collector-records")
                .setName("publisher"))
    }
    private val record = MessageSource.createRecord("metal-retrieval", "metal-json", JsonArray::class.java.name)

    @Throws(Exception::class)
    override fun start(startFuture: Future<Void>) {
        initialize()
        vertx.setPeriodic(config().getLong("delay", 30000)) {
            subscribeToEndpoint()
            showKnownRecords()
        }
        log.debug("config: {}", config().encode())
        if (!startFuture.isComplete) startFuture.complete()
    }

    @Throws(Exception::class)
    override fun stop(stopFuture: Future<Void>) {
        log.info("Stopping retrieval verticle...")
        log.info("un publish record: {} \nname: {}", record.registration, record.name)
        discovery.rxUnpublish(record.registration).subscribe({
            log.info("un-published record to service discovery..")
            discovery.close()
            compositeDisposable.dispose()
            stopFuture.complete()
        }, {
            log.error("failed to un publish")
            stopFuture.fail(it)
        })

    }

    fun initialize() {
        log.info("Starting retrieval verticle...")
        compositeDisposable.add(discovery.rxPublish(record).subscribe({ rec -> log.info("Service published, info: {}", rec.toJson()) }, { err -> log.error("Failed to publish record to service discovery", err) }))
    }

    private fun showKnownRecords() {
        discovery.getRecords(json { obj { "*" to "*" } }) { ar ->
            if (ar.succeeded()) {
                log.debug("known records {}", ar.result().map { res -> res.toJson() }.toList())
            }
        }
    }

    /**
     * Get body from the URL
     * @return a Single containing the Json body
     */
    private fun retrieveMetals(): Single<HttpResponse<JsonArray>> {
        val port = config().getInteger("port", 8080)
        val host = config().getString("host", "localhost")
        val url = config().getString("url", "/")
        return webClient
                .get(port, host, url)
                //escape 'as'
                .`as`(BodyCodec.jsonArray())
                .rxSend()
    }

    /**
     * Subscribe to retrieveMetals return value
     */
    fun subscribeToEndpoint() {
        compositeDisposable.add(
                retrieveMetals().subscribe(onSuccess(), onError()))
    }

    fun onError(): (Throwable) -> Unit = { log.error("Failed to retrieve data", it.message) }

    /**
     * handle
     */
    fun onSuccess(): (HttpResponse<JsonArray>) -> Unit = {
        log.debug("Json response: {}", it.body())
        bus.publish("metal-json", it.body())
    }

    companion object {
        private val log = LoggerFactory.getLogger(MetalRetrievalVerticle::class.java)
    }
}