package com.gitlab.vincentfree.argentax.collector;

import com.gitlab.vincentfree.argentax.collector.verticles.MetalPublishVerticle;
import com.hazelcast.config.Config;
import io.reactivex.Single;
import io.reactivex.disposables.CompositeDisposable;
import io.vertx.core.DeploymentOptions;
import io.vertx.core.Verticle;
import io.vertx.core.VertxOptions;
import io.vertx.core.json.JsonObject;
import io.vertx.core.spi.cluster.ClusterManager;
import io.vertx.reactivex.core.RxHelper;
import io.vertx.reactivex.core.Vertx;
import io.vertx.spi.cluster.hazelcast.ConfigUtil;
import io.vertx.spi.cluster.hazelcast.HazelcastClusterManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

/**
 * VertxApplication
 */
public class VertxApplication {

    private static final Logger log = LoggerFactory.getLogger(VertxApplication.class);
    private static CompositeDisposable compositeDisposable = new CompositeDisposable();
    private static Map<String, String> shutdownIds = new HashMap<>();

    @SuppressWarnings("Duplicates")
    public static void main(String[] args) {
        log.info("Initiating publisher...");
        System.setProperty("vertx.hazelcast.config", "./cluster.xml");
        int port = (System.getenv().keySet().stream().anyMatch(s -> s.equalsIgnoreCase("port"))) ? Integer.parseInt(System.getenv("port")) : 8080;
        log.info("service port: {}", port);
        Config hazelcastConfig = ConfigUtil.loadConfig();
        if (System.getenv().keySet().stream().anyMatch(s -> s.equalsIgnoreCase("hazelcastlocal"))) {
            //TODO non multicast manager
            hazelcastConfig = new Config().setProperty("hazelcast.logging.type", "slf4j");
        }
        ClusterManager mgr = new HazelcastClusterManager(hazelcastConfig);

        VertxOptions options = new VertxOptions().setClusterManager(mgr);
        Vertx vertx = Vertx.rxClusteredVertx(options).blockingGet();

        Verticle metalPublishVerticle = new MetalPublishVerticle();
        JsonObject publishOptions = new JsonObject()
                .put("port", port);

        DeploymentOptions deploymentOptions = new DeploymentOptions().setConfig(publishOptions);
        Single<String> metalPublishDeployment = RxHelper.deployVerticle(vertx, metalPublishVerticle, deploymentOptions);
        startVerticle("metalPublish", metalPublishDeployment);

        final Thread mainThread = Thread.currentThread();
        Runtime.getRuntime().addShutdownHook(new Thread() {
            public void run() {
                log.info("Shutting down...");
                vertx.undeploy(shutdownIds.get("metalPublish"));
                compositeDisposable.dispose();
                log.info("Finished shutting down...");
                try {
                    mainThread.join();
                } catch (InterruptedException e) {
                    mainThread.interrupt();
                }
            }
        });
    }

    private static void startVerticle(String name, Single<String> deployment) {
        compositeDisposable.add(deployment.subscribe(id -> {
                    log.info(id);
                    shutdownIds.put(name, id);
                }
                , err -> log.error(name + " verticle failed to deploy!", err)));
    }
}