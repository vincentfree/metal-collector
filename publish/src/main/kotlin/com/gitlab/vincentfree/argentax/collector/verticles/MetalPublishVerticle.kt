package com.gitlab.vincentfree.argentax.collector.verticles

import io.reactivex.disposables.CompositeDisposable
import io.vertx.core.Future
import io.vertx.core.http.HttpMethod
import io.vertx.core.json.JsonArray
import io.vertx.ext.healthchecks.HealthCheckHandler
import io.vertx.ext.healthchecks.HealthChecks
import io.vertx.kotlin.core.json.json
import io.vertx.kotlin.core.json.obj
import io.vertx.kotlin.servicediscovery.ServiceDiscoveryOptions
import io.vertx.reactivex.core.AbstractVerticle
import io.vertx.reactivex.ext.web.Route
import io.vertx.reactivex.ext.web.Router
import io.vertx.reactivex.servicediscovery.ServiceDiscovery
import io.vertx.reactivex.servicediscovery.types.HttpEndpoint
import io.vertx.reactivex.servicediscovery.types.MessageSource
import io.vertx.spi.cluster.hazelcast.ClusterHealthCheck
import org.slf4j.LoggerFactory
import java.util.concurrent.TimeUnit

class MetalPublishVerticle : AbstractVerticle() {

    private val compositeDisposable = CompositeDisposable()
    private val discovery: ServiceDiscovery by lazy {
        ServiceDiscovery.create(vertx, ServiceDiscoveryOptions()
                .setAnnounceAddress("collector-records")
                .setName("publisher"))
    }
    private val server by lazy { vertx.createHttpServer() }
    private val record by lazy { HttpEndpoint.createRecord("metal-publisher", "localhost", 8080, null) }
    private val procedure by lazy { ClusterHealthCheck.createProcedure(vertx.delegate) }
    private val checks by lazy { HealthChecks.create(vertx.delegate).register("cluster-health", procedure) }
    private val jsonArray = JsonArray()

    @Throws(Exception::class)
    override fun start(startFuture: Future<Void>) {
        startServer(startFuture)
        vertx.setPeriodic(30000) { showKnownRecords() }

        val eventBusAddress = MessageSource.rxGetConsumer<JsonArray>(discovery) { record -> "metal-retrieval" == record.name }
        // Get messages from metal retrieval bus
        compositeDisposable.add(eventBusAddress
                .map { it.toObservable() }
                .subscribe({ entry ->
                    entry
                            .map { retrievalPayload -> retrievalPayload.body() }
                            .subscribe({ jsonArray.add(it) }) { log.error("failed", it) }
                },
                        {
                            log.error("failed to connect to event bus", it)
                            discovery.getRecords(json { obj { "*" to "*" } }) { ar ->
                                if (ar.succeeded()) {
                                    log.warn("records: {}", ar.result().map { res -> res.toJson() }.toList())
                                } else {
                                    log.error("failed to get records from discovery")
                                    //TODO System.exit(1)
                                }
                            }
                        }))

        vertx.setPeriodic(60000) {
            var x = 0
            while ( x < jsonArray.size() /2) {
                jsonArray.remove(x--)
            }
        }
    }

    @Throws(Exception::class)
    override fun stop(stopFuture: Future<Void>) {
        log.info("Stopping publish verticle...")
        log.info("un publish record: {} \nname: {}", record.registration, record.name)
        discovery.rxUnpublish(record.registration).subscribe({
            log.info("un-published record to service discovery..")
            compositeDisposable.dispose()
            stopFuture.complete()
        }, {
            log.error("failed to un publish")
            stopFuture.fail(it)
        })
    }

    private fun createRoutes(): Router {
        val router = Router.router(vertx)
        readinessProbe(router)
        publishResultsRoute(router)
        publishLatestResultRoute(router)
        return router
    }

    /**
     * Endpoint <strong><pre>"/"</pre></strong>
     */
    private fun publishLatestResultRoute(router: Router): Route {
        return router
                .route(HttpMethod.GET, "/")
                .produces("Application/json")
                .handler { event ->
                    event.response()
                            .putHeader("content-type", "Application/json")
                            .end(getLastResult())
                }
    }

    private fun getLastResult(): String {
        return if (jsonArray.isEmpty) {
            json { obj { "value" to "empty" } }.encode()
        } else {
            jsonArray.getJsonArray((jsonArray.size() - 1)).encode()
        }
    }

    /**
     * Endpoint "/all"
     */
    private fun publishResultsRoute(router: Router): Route {
        return router
                .route(HttpMethod.GET, "/all")
                .produces("Application/json")
                .handler { event ->
                    event.response()
                            .putHeader("content-type", "Application/json")
                            .end(jsonArray.encode())
                }
    }

    /**
     * Endpoint "/readiness"
     */
    private fun readinessProbe(router: Router): Route {
        return router.get("/readiness").handler {
            HealthCheckHandler.createWithHealthChecks(checks).handle(it.delegate)
        }
    }

    private fun showKnownRecords() {
        discovery.getRecords(json { obj { "*" to "*" } }) { ar ->
            if (ar.succeeded()) {
                log.debug("known records {}", ar.result().map { res -> res.toJson() }.toList())
            }
        }
    }

    private fun startServer(startFuture: Future<Void>) {
        val router = createRoutes()
        val port = config().getInteger("port", 8080)

        // Set up service discovery
        server.exceptionHandler { err -> log.error("Internal error setting up connection", err) }
                .requestHandler(router)

        compositeDisposable.addAll(discovery.rxPublish(record)
                .timeout(1, TimeUnit.SECONDS)
                .retry(5)
                .subscribe({ log.info("Published record with name: {}", it.name) }, { log.error("Failed to log record") }),
                server.rxListen(port)
                        .subscribe(
                                {
                                    log.info("subscribed to http server")
                                    startFuture.complete()
                                },
                                { err ->
                                    log.error("failed to process request", err)
                                    startFuture.fail(err)
                                }
                        )
        )
    }

    companion object {
        private val log = LoggerFactory.getLogger(MetalPublishVerticle::class.java)
    }
}
