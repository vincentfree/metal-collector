package com.gitlab.vincentfree.argentax.collector.metals

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.gitlab.vincentfree.argentax.collector.constants.Currency
import com.gitlab.vincentfree.argentax.collector.constants.MetalType

/**
 * Created by Vincent Free on 25-5-2017
 */
@JsonIgnoreProperties(ignoreUnknown = true)
interface MetalQuote {

    val type: MetalType

    val price: Double

    val currency: Currency

    val logTime: Long
}
