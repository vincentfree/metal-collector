package com.gitlab.vincentfree.argentax.collector.metals

import io.vertx.core.json.JsonArray
import io.vertx.core.json.JsonObject

class MetalList {
    private val metalJsonArray = JsonArray()

    fun addSample(json: JsonObject) {
        metalJsonArray.add(json)
    }
}
