package com.gitlab.vincentfree.argentax.collector.constants

enum class MetalType {
    Gold, Silver, Palladium, Platinum
}