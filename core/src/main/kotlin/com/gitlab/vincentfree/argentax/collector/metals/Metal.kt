package com.gitlab.vincentfree.argentax.collector.metals

import com.gitlab.vincentfree.argentax.collector.constants.Currency
import com.gitlab.vincentfree.argentax.collector.constants.MetalType
import io.vertx.core.json.JsonObject

data class Metal(override val type: MetalType, override val price: Double
                 , override val currency: Currency
                 , override val logTime: Long = System.currentTimeMillis()) : MetalQuote {
    fun toJson(): JsonObject = JsonObject.mapFrom(this)
}