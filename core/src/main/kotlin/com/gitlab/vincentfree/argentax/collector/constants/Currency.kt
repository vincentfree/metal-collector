package com.gitlab.vincentfree.argentax.collector.constants

enum class Currency {
    EUR, USD, GBP
}