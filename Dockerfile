# syntax = docker/dockerfile:experimental
FROM gradle:5.0-jdk8 as build
COPY --chown=gradle . /home/gradle/project
ENV CACHE=/home/gradle/.gradle/caches
ARG NAME=retrieval
WORKDIR /home/gradle/project/${NAME}
RUN --mount=type=cache,target=$CACHE gradle build :$NAME:shadowJar
USER root
RUN mkdir -p -m 777 /home/artifact && cp /home/gradle/project/$NAME/build/libs/$NAME-*-all.jar /home/artifact/app.jar

FROM gcr.io/distroless/java
ENV NAME=$NAME
COPY cluster.xml .
COPY --from=build /home/artifact/app.jar ./app.jar
CMD ["app.jar"]